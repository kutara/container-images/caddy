#!/usr/bin/env bash

set -x

# Use this file for assumptions within Gitlab CI.

# renovate: datasource=github-tags depName=caddyserver/caddy
export CADDY_VERSION=v2.4.2

export IMAGE_BUILD=docker.io/library/caddy@sha256:01a8ada44e16265f5d3fb087a06b76fddb82231212c73552403c14e48ae34f82
export IMAGE=gcr.io/distroless/static@sha256:be5d77c62dbe7fedfb0a4e5ec2f91078080800ab1f18358e5f31fcc8faa023c4

ctr0=$(buildah from --arch "$ARCH" "$IMAGE_BUILD")
ctr1=$(buildah from --arch "$ARCH" "$IMAGE")

buildah config --env XCADDY_SETCAP=1 --env CADDY_VERSION=$CADDY_VERSION "$ctr0"

buildah run "$ctr0" sh -c "
xcaddy build --with \
github.com/caddy-dns/cloudflare \
"

buildah add --chown nonroot:nonroot "$ctr1" https://github.com/caddyserver/dist/raw/a8ef04588bf34a9523b76794d601c6e9cb8e31d3/config/Caddyfile /etc/caddy/Caddyfile
buildah add --chown nonroot:nonroo "$ctr1" https://github.com/caddyserver/dist/raw/a8ef04588bf34a9523b76794d601c6e9cb8e31d3/welcome/index.html /usr/share/caddy/index.html

buildah copy --from "$ctr0" "$ctr1" /usr/bin/caddy /caddy

buildah config \
--env XDG_CONFIG_HOME=1\
--env XDG_DATA_HOME=/data \
"$ctr0"

buildah config --volume /config "$ctr1"
buildah config --volume /data "$ctr1"

buildah config \
--port 80 \
--port 443 \
--port 2019 \
"$ctr1"

buildah config --user nonroot "$ctr1"

buldah config \
--label org.opencontainers.image.version=$CADDY_VERSION \
--label org.opencontainers.image.title=Caddy \
--label org.opencontainers.image.description="a powerful, enterprise-ready, open source web server with automatic HTTPS written in Go" \
--label org.opencontainers.image.url=https://caddyserver.com \
--label org.opencontainers.image.documentation=https://caddyserver.com/docs \
--label org.opencontainers.image.vendor="Light Code Labs" \
--label org.opencontainers.image.licenses=Apache-2.0 \
--label org.opencontainers.image.source="https://gitlab.com/kutara/container-images/caddy" \
--label io.containers.autoupdate \
--label io.kutara.version.caddy=$CADDY_VERSION \
"$ctr1"


buildah config --cmd '["/caddy", "run", "--config", "/etc/caddy/Caddyfile", "--adapter", "caddyfile"]' "$ctr1"
buildah rm "$ctr0"
buildah commit --rm "$ctr1" caddy
