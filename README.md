# caddy

distroless Caddy compiled with cloudflare-dns plugin

```sh
podman run --name caddy --net=host \
                  -v /etc/caddy/Caddyfile:/config/Caddyfile:z \
                  -v /var/data:/data \
                  --user root \
                  quay.io/kutara/caddy:latest /caddy run --config /config/Caddyfile --adapter caddyfile
```

**note**: Until https://github.com/containers/buildah/pull/3320 is tagged this container needs to be ran as roon

Example caddyfile

```
s3.nwk1.rabbito.tech {
        reverse_proxy http://nas-1.nwk1.rabbito.tech:9000 {
                header_up X-Forwarded-Host {host}
                header_up Host {host}
        }
}
netboot.nwk1.rabbito.tech {
        reverse_proxy http://127.0.0.1:8080
        tls {
                dns cloudflare ${CF_API_KEY}
        }
}
```
